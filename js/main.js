const engWord = document.getElementById('eng'),
    rusWord = document.getElementById('rus'),
    inputs = document.getElementsByClassName('input'),
    addButton = document.getElementById('add-word-btn'),
    table = document.getElementById('table');

let words,
    btnsDelete;

localStorage.length < 1 ? words = [] : words = JSON.parse(localStorage.getItem('words'));


const addEventDelete = () => {
    if(words.length > 1) {
        btnsDelete = document.querySelectorAll('.btn-delete');
        for(let btn of btnsDelete) {
            btn.addEventListener('click', e => {
                deleteWord(e);
            });
        }
    }
}


const addWordToTable = index => {
    table.innerHTML +=`
        <tr class="tr">
            <td class="eng-word">${words[index].english}</td>
            <td class="rus-word">${words[index].russian}</td>
            <td>
                <button class="btn-delete"></button>
            </td>
        </tr>
    `
    addEventDelete();
}
    
words.forEach((element, index) => {
    addWordToTable(index);
});

addButton.addEventListener('click', () => {
    if(
        engWord.value.length < 1 ||
        rusWord.value.length < 1 ||
        !isNaN(engWord.value) ||
        !isNaN(engWord.value)
        ) {
            for(let input of inputs) {
                input.classList.add('error');
        }
    } else {
        for(let input of inputs) {
            input.classList.remove('error');
        }
        words.push(new CreateWord(engWord.value, rusWord.value));
        localStorage.setItem('words', JSON.stringify(words));
        addWordToTable(words.length - 1);
        engWord.value = null;
        rusWord.value = null;
    }
})


function CreateWord (english, russian) {
    this.english = english;
    this.russian = russian;
}

const deleteWord = e => {
    const rowIndex = e.target.parentNode.parentNode.rowIndex;
    e.target.parentNode.parentNode.parentNode.remove();
    words.splice(rowIndex, 1);
    localStorage.removeItem('words');
    localStorage.setItem('words', JSON.stringify(words));
}

addEventDelete();

function resizeText() {
    let text = document.getElementsByClassName('text-gradient');
    for (let i=0; i<text.length; i++) {
        let string = text[i].innerHTML;
        string = string.replace(/<[^>]+>/g,'');
        text[i].style.fontSize = (text[i].offsetWidth / string.length * 1.1).toFixed() + "px";
        text[i].innerHTML = string.replace(/(.)/g, '<span style="min-width: '+ 
        (text[i].offsetWidth / string.length / 1.4).toFixed() +'px">$1</span>');
    }
}

window.addEventListener("resize", resizeText);
resizeText();